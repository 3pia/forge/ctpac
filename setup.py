from setuptools import setup, Extension
import numpy as np

tpc = Extension('_tpc', sources=['tpc.c'], extra_compile_args=['-fopenmp', '-Ofast', '-g', '-march=native', '-std=c11'],
      extra_link_args=['-lgomp'])

setup (
       name='tpc',
       version='1.1',
       description='2pt (auto-)correlation',
       author='Gero Mueller, David Walz',
       author_email='gero.mueller@physik.rwth-aachen.de',
       url='',
       long_description='2pt autocorrelation for astro using numpy and openmp',
       include_dirs=[np.get_include()],
       ext_modules=[tpc],
       py_modules = ['tpc'],
       test_suite = "test",
)
