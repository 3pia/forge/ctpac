import unittest
import doctest
import sys
import os

import tpc

import numpy as np
from matplotlib import pyplot as plt, colors
from  scipy.stats import poisson, binom


def isotropy(n):
    phi = (2 * np.random.rand(n) - 1) * np.pi
    theta = np.arccos(2 * np.random.rand(n) - 1) - (np.pi / 2)

    return ang2vec(phi, theta)

def plot_thpac_hist(r, gammabins, zetabins, filename=None):
    aspect = float((gammabins[-1] - gammabins[0])/(zetabins[-1] - zetabins[0]))
    gammastep = (gammabins[-1] - gammabins[0]) / r.shape[0]
    zetastep = (zetabins[-1] - zetabins[0]) / r.shape[1]
    extent = (gammabins[0] + gammastep/2, gammabins[-1] - gammastep/2, zetabins[0] + zetastep/2, zetabins[-1] - zetastep/2,)
    plt.imshow(r, origin="lower", extent=extent, interpolation='none', norm=colors.LogNorm(vmax=max(2, r.max())), aspect=aspect )
    plt.colorbar()
    plt.xlabel("$\gamma \ (Symmetric)$")
    plt.ylabel("$\zeta \ (Concentration)$")
    plt.grid(alpha=0.25)
    if filename:
        plt.savefig(filename)
        plt.close("all")

def plot_thpac_poisson(p, gammabins, zetabins, filename=None):
    plt.imshow(p, origin="lower", extent=(gammabins[0], gammabins[-1], zetabins[0], zetabins[-1]), interpolation='none')
    plt.colorbar()
    plt.xlabel("$\gamma \ (Symmetric)$")
    plt.ylabel("$\zeta \ (Concentration)$")
    plt.grid(alpha=0.25)
    if filename:
        plt.savefig(filename)
        plt.close("all")
        
def vec2ang(d):
    phi = np.arctan2(d[1], d[0])
    theta = np.arctan2(d[2], (d[0] * d[0] + d[1] * d[1]) ** .5)
    return phi, theta

def ang2vec(phi, theta):
    result = np.zeros((3, len(phi)))
    result[0] = np.cos(theta) * np.cos(phi)
    result[1] = np.cos(theta) * np.sin(phi)
    result[2] = np.sin(theta)
    return result

def plot_sky(d, mark=0, subplot=None, filename=None):
    subplot = 111 if subplot is None else subplot
    plt.subplot(subplot, projection="hammer")
    phi, theta = vec2ang(d)
    plt.scatter(phi[mark:], theta[mark:], color='b')
    plt.scatter(phi[:mark], theta[:mark], color='r')
    plt.grid(alpha=0.25)
    if filename:
        plt.savefig(filename)
        plt.close("all")

def plot_side_by_side(data, r, filename, mark=0):
    plt.figure(figsize=(10, 4))
    plt.subplot(121)
    plot_thpac_hist(*r)
    plot_sky(data, mark=mark, subplot=122)

    plt.savefig(filename)
    plt.tight_layout()
    plt.close("all")
    
def plot_overview(data, r, p, b, filename, mark=0, pvalue=None):
    plt.figure(figsize=(10, 8))
    plt.subplot(221)
    plot_thpac_hist(*r)
    plot_sky(data, mark=mark, subplot=222)

    plt.subplot(223)
    plot_thpac_hist(p, r[1], r[2])
    if pvalue is not None:
        plt.title("%g" % pvalue)
    #plt.subplot(224)
    #plot_thpac_hist(b, r[1], r[2])

    plt.savefig(filename)
    plt.tight_layout()
    plt.close("all")
        
    
class BasicTest( unittest.TestCase ):
    
    def test_concentrated_symmetric(self):
        phi = np.radians(np.array([-10, 10, 0]))
        theta = np.radians(np.array([-10, -10, 10]))
        data = ang2vec(phi, theta)
        r = tpc.thpac(data[0], data[1], data[2])
        plot_side_by_side(data, r, "basic_concentrated_symmetric.png")

    def test_spread_symmetric(self):
        phi = np.radians(np.array([-60, 60, 0]))
        theta = np.radians(np.array([-50, -50, 50]))
        data = ang2vec(phi, theta)
        r = tpc.thpac(data[0], data[1], data[2])
        plot_side_by_side(data, r, "basic_spread_symmetric.png")

    def test_spread_symmetric_var(self):
        p = np.array([-60, 60, 0])
        t = np.array([-50, -50, 50])
        phi = np.radians(np.r_[p, p*1.01, p*0.99, p*1.02, p*0.98])
        theta = np.radians(np.r_[t, t*1.01, t*0.99, t*1.02, t*0.98])
        data = ang2vec(phi, theta)
        r = tpc.thpac(data[0], data[1], data[2])
        plot_side_by_side(data, r, "basic_spread_symmetric_var.png")

    def test_spread_symmetric_4(self):
        phi = np.radians(np.array([-60, 60, 0, 0]))
        theta = np.radians(np.array([-50, -50, 50, 0]))
        data = ang2vec(phi, theta)
        r = tpc.thpac(data[0], data[1], data[2])
        plot_side_by_side(data, r, "basic_spread_symmetric_4.png")

    def test_concentrated_elonged(self):
        phi = np.radians(np.array([-10, 10, 0]))
        theta = np.radians(np.array([-5, -5, 0]))
        data = ang2vec(phi, theta)
        r = tpc.thpac(data[0], data[1], data[2])
        plot_side_by_side(data, r, "basic_concentrated_elonged.png")

    def test_spread_elonged(self):
        phi = np.radians(np.array([-60, 60, 0]))
        theta = np.radians(np.array([-10, -10, 10]))
        data = ang2vec(phi, theta)
        r = tpc.thpac(data[0], data[1], data[2])
        plot_side_by_side(data, r, "basic_spread_elonged.png")

    def test_spread_very_elonged(self):
        phi = np.radians(np.array([-30, 30, -60]))
        theta = np.radians(np.array([-10, -10, 20]))
        data = ang2vec(phi, theta)
        r = tpc.thpac(data[0], data[1], data[2])
        plot_side_by_side(data, r, "basic_spread_very_elonged.png")
        
    def test_line(self):
        phi = np.radians(np.array([-60, 60, 0]))
        theta = np.radians(np.array([0, 0.01, 0]))
        data = ang2vec(phi, theta)
        r = tpc.thpac(data[0], data[1], data[2])
        plot_side_by_side(data, r, "basic_line.png")

        
    def test_point(self):
        phi = np.radians(np.array([0.01, 0.01, 0.01]))
        theta = np.radians(np.array([0, 0.01, 0]))
        data = ang2vec(phi, theta)
        r = tpc.thpac(data[0], data[1], data[2])
        plot_side_by_side(data, r, "basic_point.png")

            
class IsotropyTest( unittest.TestCase ):
    
    def setUp(self):
        self.iso = isotropy(300)
        r = tpc.thpac(self.iso[0], self.iso[1], self.iso[2])
        plot_side_by_side(self.iso, r, "iso.png")

    def test_iso_spread_sym(self):

        data = self.iso.copy()
        weights=np.ones(data.shape[1])
        #weights[:4]*=10000

        phi = np.radians(np.array([-60, 60, 0, 0]))
        theta = np.radians(np.array([-50, -50, 50, 0]))
        signal = ang2vec(phi, theta)
        data[0][:4] = signal[0]
        data[1][:4] = signal[1]
        data[2][:4] = signal[2]

        r = tpc.thpac(data[0], data[1], data[2], weights=weights)
        plot_side_by_side(data, r, "iso_spread_sym.png", mark=4)
        
        
    def test_spread(self):
        data = self.iso.copy()
        for spread in np.linspace(0.01, 0.5, 5, endpoint=True):
            for i in xrange(10):
                data[:,i+1] = data[:,0] + np.random.normal(0, spread, size=3)
            r = tpc.thpac(data[0], data[1], data[2])

            plot_side_by_side(data, r, "spread-%g.png" % spread, mark=10)
        
    def test_sheet(self):
        data = self.iso.copy()
        r = tpc.thpac(data[0], data[1], data[2])
        plot_side_by_side(data, r, "sheet.png")

        for spread in np.linspace(0.01, 0.5, 5, endpoint=True):
            for i in xrange(10):
                data[:,i+1] = data[:,0] + np.r_[np.random.normal(0, spread, size=1), 1.0, 1.0]
            r = tpc.thpac(data[0], data[1], data[2])
            
            plot_side_by_side(data, r, "sheet-%g.png" % spread, mark=10)

class PoissonTest( unittest.TestCase ):
    
    def setUp(self):
        self.n = 200
        self.niso = 1000
        self.hiso, self.gbins, self.zbins = None, None, None
        if os.path.isfile("hiso.np"):
            self.gbins = np.fromfile("gbins.np")
            self.zbins = np.fromfile("zbins.np")
            self.hiso = np.fromfile("hiso.np").reshape((self.zbins.shape[0], self.gbins.shape[0]))
        else:
            print ""
            for i in xrange(self.niso):
                sys.stdout.write( "Creating isotropy... % 2.1f\r" % (100. * float(i) / self.niso))
                sys.stdout.flush()
                iso = isotropy(self.n)
                r = tpc.thpac(iso[0], iso[1], iso[2])
                if self.hiso is None:
                    (self.hiso, self.gbins, self.zbins) = r
                else:
                    self.hiso += r[0]
            print ""
            self.hiso = self.hiso.astype(np.float64) / self.niso
            self.hiso.tofile("hiso.np")
            self.gbins.tofile("gbins.np")
            self.zbins.tofile("zbins.np")
            plot_thpac_hist(self.hiso, self.gbins, self.zbins, filename="hiso.png")
        self.hiso = np.maximum(1./self.niso**2, self.hiso)
            
        if os.path.isfile("Piso.np"):
            self.Piso = np.fromfile("Piso.np")
        else:
            self.Piso = np.zeros(self.niso)
            print ""
            for i in xrange(self.niso):
                sys.stdout.write( "Creating isotropy... % 2.1f\r" % (100. * float(i) / self.niso))
                sys.stdout.flush()
                iso = isotropy(self.n)
                r = tpc.thpac(iso[0], iso[1], iso[2])
                #print r[0]
                p = poisson.pmf(r[0], self.hiso)
                #print p
                self.Piso[i] = np.sum(np.log(p))
                #print self.Piso[i]
            print ""
            self.Piso.tofile("Piso.np")

        plt.hist(self.Piso)
        plt.savefig("Piso.png")
            
                
    def test_spread(self):
        data = isotropy(self.n)
        r = tpc.thpac(data[0], data[1], data[2])
        p = poisson.pmf(r[0], self.hiso)
        P = np.sum(np.log(p))
        pvalue = float(np.sum(self.Piso < P)) / self.niso
        plot_overview(data, r, p, None, "poisson-spread.png", pvalue=pvalue)

        for spread in np.linspace(0.01, 0.5, 5, endpoint=True):
            for i in xrange(9):
                data[:,i+1] = data[:,0] + np.random.normal(0, spread, size=3)
            r = tpc.thpac(data[0], data[1], data[2])
            p = poisson.pmf(r[0], self.hiso)
            P = np.sum(np.log(p))
            pvalue = float(np.sum(self.Piso < P)) / self.niso
            #b = binom.pmf(r[0], self.niso, self.hiso / self.niso)
            plot_overview(data, r, p, None, "poisson-spread-%g.png" % spread, mark=10, pvalue=pvalue)
        
    def test_sheet(self):
        data = isotropy(self.n)
        r = tpc.thpac(data[0], data[1], data[2])
        p = poisson.pmf(r[0], self.hiso)
        P = np.sum(np.log(p))
        pvalue = float(np.sum(self.Piso < P)) / self.niso
        plot_overview(data, r, p, None, "poisson-sheet.png", pvalue=pvalue)

        for spread in np.linspace(0.01, 0.5, 5, endpoint=True):
            for i in xrange(9):
                data[:,i+1] = data[:,0] + np.r_[np.random.normal(0, spread, size=1), .0, .0]
            r = tpc.thpac(data[0], data[1], data[2])
            p = poisson.pmf(r[0], self.hiso)
            P = np.sum(np.log(p))
            pvalue = float(np.sum(self.Piso < P)) / self.niso
           
            plot_overview(data, r, p, None, "poisson-sheet-%g.png" % spread, mark=10, pvalue=pvalue)