#include "Python.h"
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include "numpy/ndarrayobject.h"
#include <math.h>
#include "dsyevc3.h"

typedef struct fast_array_s {
	size_t size;
	double *data;
	void *mem;
} fast_array_t;

int init_fast_array(fast_array_t *fa, PyArrayObject *pa) {
	if (pa == NULL || fa == NULL)
		return 0;

	fa->size = PyArray_DIM(pa, 0);

	char *data = (char *) PyArray_DATA(pa);
	npy_intp stride = PyArray_STRIDE(pa, 0) ;

	fa->mem = malloc(fa->size * sizeof(double) + 15);
	fa->data = (double *) (((uintptr_t) fa->mem + 15) & ~0x0F);
	size_t i;
	for (i = 0; i < fa->size; i++) {
		fa->data[i] = *(double *) (data + i * stride);
	}

	return 1;
}

void free_fast_array(fast_array_t *fa) {
	free(fa->mem);
	fa->data = NULL;
	fa->size = 0;
}

// Normalize vectors to 1
void normalize(size_t n, double *x, double *y, double *z) {
	size_t i;
	#pragma omp parallel for private(i), schedule(static, 1000)
	for (i = 0; i < n; i++) {
		double l = 1./sqrt(x[i]*x[i]+y[i]*y[i]+z[i]*z[i]);
		x[i] *= l;
		y[i] *= l;
		z[i] *= l;
	}
}

// Weighted two-point autocorrelation
static PyObject *wtpac(PyObject *self, PyObject *args) {
	PyArrayObject *ac, *ax, *ay, *az, *aw;
	double maxangle;

	// parse the parameters
	if (!PyArg_ParseTuple(args, "O!O!O!O!O!d",
		&PyArray_Type, &ac,
		&PyArray_Type, &ax,
		&PyArray_Type, &ay,
		&PyArray_Type, &az,
		&PyArray_Type, &aw,
		&maxangle))
		return NULL ;

	// extract parameters and arrays
	fast_array_t x, y, z, w;

	if (!init_fast_array(&x, ax))
		return NULL;
	if (!init_fast_array(&y, ay))
		return NULL;
	if (!init_fast_array(&z, az))
		return NULL;
	if (!init_fast_array(&w, aw))
		return NULL;

	if ((x.size != y.size) || (x.size != z.size) || (x.size != w.size))
		return NULL;
	size_t n = x.size;
	normalize(n, x.data, y.data, z.data);

	npy_intp nbins = PyArray_DIM(ac, 0) ;
	double *ac_data = (double *) PyArray_DATA(ac);
	npy_intp ac_stride = PyArray_STRIDE(ac, 0) ;

	size_t i, j, idx;
	double m = cos(maxangle);
	double d = nbins / maxangle;

#pragma omp parallel shared(x, y, z, n, d, nbins) private(i, j, idx)
	{
		// setup temporary result array
		double *tmp = (double *) malloc(nbins * sizeof(double));
		int ib;
		for (ib = 0; ib < nbins; ib++) {
			tmp[ib] = 0.;
		}

		// compute autocorrelation
#pragma omp for schedule(dynamic, 16) nowait
		for (i = 0; i < n - 1; i++) {
			for (j = i + 1; j < n; j++) {
				float cosalpha = x.data[i] * x.data[j] + y.data[i] * y.data[j] + z.data[i] * z.data[j];
				if (cosalpha >= m) {
					idx = acos(cosalpha) * d;
					tmp[idx] += (w.data[i] * w.data[j]);
				}
			}
		}

		// add temporary result to total result
#pragma omp critical
		for (ib = 0; ib < nbins; ib++) {
			ac_data[ib] += tmp[ib];
		}

		// cleanup
		free(tmp);
	}

	free_fast_array(&x);
	free_fast_array(&y);
	free_fast_array(&z);
	free_fast_array(&w);

	Py_INCREF(Py_None);
	return Py_None;
}

// Weighted two-point crosscorrelation
static PyObject *wtpcc(PyObject *self, PyObject *args) {
	PyArrayObject *ac, *ax1, *ay1, *az1, *aw1, *ax2, *ay2, *az2, *aw2;
	double maxangle;

	// parse the parameters
	if (!PyArg_ParseTuple(args, "O!O!O!O!O!O!O!O!O!d",
			&PyArray_Type, &ac,
			&PyArray_Type, &ax1,
			&PyArray_Type, &ay1,
			&PyArray_Type, &az1,
			&PyArray_Type, &aw1,
			&PyArray_Type, &ax2,
			&PyArray_Type, &ay2,
			&PyArray_Type, &az2,
			&PyArray_Type, &aw2,
			&maxangle))
		return NULL ;

	// extract parameters and arrays
	fast_array_t x1, y1, z1, w1, x2, y2, z2, w2;

	if (!init_fast_array(&x1, ax1))
		return NULL;
	if (!init_fast_array(&y1, ay1))
		return NULL;
	if (!init_fast_array(&z1, az1))
		return NULL;
	if (!init_fast_array(&w1, aw1))
		return NULL;
	if (!init_fast_array(&x2, ax2))
		return NULL;
	if (!init_fast_array(&y2, ay2))
		return NULL;
	if (!init_fast_array(&z2, az2))
		return NULL;
	if (!init_fast_array(&w2, aw2))
		return NULL;

	if ((x1.size != y1.size) || (x1.size != z1.size) || (x1.size != w1.size))
		return NULL;
	size_t n1 = x1.size;
	normalize(n1, x1.data, y1.data, z1.data);

	if ((x2.size != y2.size) || (x2.size != z2.size) || (x2.size != w2.size))
		return NULL;
	size_t n2 = x2.size;
	normalize(n2, x2.data, y2.data, z2.data);

	npy_intp nbins = PyArray_DIM(ac, 0) ;
	double *ac_data = (double *) PyArray_DATA(ac);
	npy_intp ac_stride = PyArray_STRIDE(ac, 0) ;

	size_t i1, i2, idx;
	double m = cos(maxangle);
	double d = nbins / maxangle;

#pragma omp parallel shared(x1, y1, z1, w1, x2, y2, z2, w2, n1, n2, d, nbins) private(i1, i2, idx)
	{
		// setup temporary result array
		double *tmp = (double *) malloc(nbins * sizeof(double));
		int ib;
		for (ib = 0; ib < nbins; ib++) {
			tmp[ib] = 0.;
		}

		// compute correlation
#pragma omp for schedule(dynamic, 16) nowait
		for (i1 = 0; i1 < n1; i1++) {
			for (i2 = 0; i2 < n2; i2++) {
				float cosalpha = x1.data[i1] * x2.data[i2] + y1.data[i1] * y2.data[i2] + z1.data[i1] * z2.data[i2];
				if (cosalpha >= m) {
					idx = acos(cosalpha) * d;
					tmp[idx] += w1.data[i1] * w2.data[i2];
				}
			}
		}

		// add temporary result to total result
#pragma omp critical
		for (ib = 0; ib < nbins; ib++) {
			ac_data[ib] += tmp[ib];
		}

		// cleanup
		free(tmp);
	}

	free_fast_array(&x1);
	free_fast_array(&y1);
	free_fast_array(&z1);
	free_fast_array(&w1);
	free_fast_array(&x2);
	free_fast_array(&y2);
	free_fast_array(&z2);
	free_fast_array(&w2);

	Py_INCREF(Py_None);
	return Py_None;
}

int lower_bound (const double *a, int size, double val) {
   int lo = 0, hi = size - 1;
   while (lo < hi) {
      int mid = lo + (hi - lo)/2;
      if (a[mid] < val)
         lo = mid + 1;
      else
         hi = mid;
   }
   return lo;
}



#define T(a, b) (a[i] * b[i] + a[j] * b[j] + a[k] * b[k]) // / 3. opt
#define SWAP_IF_LESS(a, b) if(a < b) {double t = a; a = b; b = t;}

// Weighted three-point autocorrelation
static PyObject *wthpac(PyObject *self, PyObject *args) {
	PyArrayObject *hist, *ax, *ay, *az, *aw, *azetabins, *agammabins;

	// parse the parameters
	if (!PyArg_ParseTuple(args, "O!O!O!O!O!O!O!",
		&PyArray_Type, &hist,
		&PyArray_Type, &ax,
		&PyArray_Type, &ay,
		&PyArray_Type, &az,
		&PyArray_Type, &aw,
		&PyArray_Type, &azetabins,
		&PyArray_Type, &agammabins))
		return NULL ;

	// extract parameters and arrays
	fast_array_t x, y, z, w, fzetabins, fgammabins;

	if (!init_fast_array(&x, ax))
		return NULL;
	if (!init_fast_array(&y, ay))
		return NULL;
	if (!init_fast_array(&z, az))
		return NULL;
	if (!init_fast_array(&w, aw))
		return NULL;
	if (!init_fast_array(&fzetabins, azetabins))
		return NULL;
	if (!init_fast_array(&fgammabins, agammabins))
		return NULL;
    
    double *zetabins = fzetabins.data;
    double *gammabins = fgammabins.data;
    
	if ((x.size != y.size) || (x.size != z.size) || (x.size != w.size))
		return NULL;
	size_t n = x.size;
	normalize(n, x.data, y.data, z.data);

	npy_intp nzetabins = PyArray_DIM(azetabins, 0) ;
	npy_intp ngammabins = PyArray_DIM(agammabins, 0) ;
    size_t nhistbins = nzetabins * ngammabins;
    
    double *hist_data = (double *) PyArray_DATA(hist);
	npy_intp hist_stride = PyArray_STRIDE(hist, 0) ;

    size_t i, j, k;
    
#pragma omp parallel shared(x, y, z, n, nhistbins) private(i, j, k)
	{
		// setup temporary result array
		double *tmp_hist_data = (double *) malloc(nhistbins * sizeof(double));
		for (size_t ib = 0; ib < nhistbins; ib++) {
			tmp_hist_data[ib] = 0.;
		}
        
        double A[3][3];
        double Q[3][3];
        double E[3];
        
        double *X = x.data;
        double *Y = y.data;
        double *Z = z.data;
        double *W = w.data;
        
        
		// compute autocorrelation
#pragma omp for schedule(dynamic, 16) nowait
		for (size_t i = 0; i < n; i++) {
			for (size_t j = i + 1; j < n; j++) {
                for (size_t k = j + 1; k < n; k++) {
                    // fill T matrix
                    A[0][0] = T(X, X);
                    A[0][1] = T(X, Y);
                    A[0][2] = T(X, Z);

                    //A[1][0] = T(Y, X); //opt
                    A[1][1] = T(Y, Y);
                    A[1][2] = T(Y, Z);

                    //A[2][0] = T(Z, X); // opt
                    //A[2][1] = T(Z, Y); // opt
                    A[2][2] = T(Z, Z);
                    
                    // calculate eigenvalues
                    dsyevc3(A, E);
                    //dsyevq3(A, Q, E);

                    // fix negative values -0
                    for (size_t s = 0; s < 3; s++)
                        if (E[s] < 0)
                            E[s] = 0;
                    
//                     if (E[0] * E[1] * E[2] < 0) {
//                         printf("%f %f %f\n", E[0], E[1], E[2]);
//                         printf("%f, %f, %f\n", X[i], X[j], X[k]);
//                         printf("%f, %f, %f\n", Y[i], Y[j], Y[k]);
//                         printf("%f, %f, %f\n", Z[i], Z[j], Z[k]);
//                     }
//                     
                    // sort eigenvalues
                    SWAP_IF_LESS(E[0], E[1])
                    SWAP_IF_LESS(E[1], E[2])
                    SWAP_IF_LESS(E[0], E[1])
                    
                    //printf("%f %f %f\n", E[0], E[1], E[2]);
                    
//                     if (E[0] < 0) printf("E0 < 0: %f\n", E[0]);
//                     if (E[1] < 0) printf("E1 < 0: %f\n", E[1]);
//                     if (E[2] < 0) printf("E2 < 0: %f\n", E[2]);
                    
                    // calculate observables
                    double zeta = log10(E[0]/E[2]);
                    double gamma = log10(log10(E[0]/E[1]) / log10(E[1]/E[2]));
                    
                    // histogram
                    int zidx = lower_bound(zetabins, nzetabins, zeta) - 1;
//                     if (zidx < 0) {
//                         printf("zidx < 0: %f - %f %f %f\n", zeta, E[0], E[1], E[2]);
//                         continue;
//                     } else if (zidx > nzetabins) {
//                         printf("zidx > nzetabins\n");
//                         continue;
//                     }
//                         
                    //printf("zeta: %f %f %f\n", zeta, zetabins[zidx], zetabins[zidx+1]);
                    int gidx = lower_bound(gammabins, ngammabins, gamma) - 1;
//                     if (gidx < 0) {
//                         printf("gidx < 0: %f", gamma);
//                         continue;
//                     } else if (gidx > ngammabins) {
//                         printf("gidx > ngammabins\n");
//                         continue;
//                     }

                    tmp_hist_data[zidx * ngammabins + gidx] += W[i] * W[j] * W[k];

                }
			}
		}

		// add temporary result to total result
#pragma omp critical
		for (size_t  ib = 0; ib < nhistbins; ib++) {
			hist_data[ib] += tmp_hist_data[ib];
		}

		// cleanup
		free(tmp_hist_data);
	}

	free_fast_array(&x);
	free_fast_array(&y);
	free_fast_array(&z);
	free_fast_array(&w);

	Py_INCREF(Py_None);
	return Py_None;
}

static PyMethodDef _tpcMethods[] = {
	{ "wtpac", wtpac, METH_VARARGS },
	{ "wtpcc", wtpcc, METH_VARARGS },
	{ "wthpac", wthpac, METH_VARARGS },
	{ NULL,		NULL, 0 }
};

void init_tpc() {
	Py_InitModule("_tpc", _tpcMethods);
	import_array();
}
