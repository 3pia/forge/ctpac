import numpy as np
import healpy
import _tpc


def tpac(x, y, z, maxangle, nbins, **kwargs):
    """
    Angular two-point auto-correlation with optional weights.

    Parameters
    ----------
    maxangle: float
        maximum angular separation in [rad]
    nbins: int
        number of angular bins
        bin edges are given by linspace(0, maxangle, nbins+1)
    weights: array_like, optional
        weight for each event
    cumulative: bool
        cumulative autocorrelation, default: True
    normalized: bool
        normalize S(pi) = 1, default: False

    Returns
    -------
    ac: ndarray
        histogram of the angular distances of pairs of vectors x,y,z

    See Also
    --------
    astrotools.twoPtAuto
    """
    # optional weights
    w = kwargs.get('weights', np.ones(len(x)))

    ac = np.zeros(nbins)
    _tpc.wtpac(ac, x, y, z, w, maxangle)

    if kwargs.get("cumulative", True):
        ac = np.cumsum(ac)
    if kwargs.get("normalized", False):
        ac /= (sum(w)**2 - sum(w**2)) / 2
    return ac

def tpcc(x1, y1, z1, x2, y2, z2, maxangle, nbins, **kwargs):
    """
    Angular two-point cross-correlation with optional weights.

    Parameters
    ----------
    maxangle: float
        maximum angular separation in [rad]
    nbins: int
        number of angular bins
        bin edges are given by linspace(0, maxangle, nbins+1)
    weights: array_like, optional
        weight for each event
    cumulative: bool
        cumulative autocorrelation, default: True
    normalized: bool
        normalize S(pi) = 1, default: False

    Returns
    -------
    cc: ndarray
        histogram of the angular distances of pairs of vectors

    See Also
    --------
    astrotools.twoPtCross
    """
    # optional weights
    w1 = kwargs.get('weights1', np.ones(len(x1)))
    w2 = kwargs.get('weights2', np.ones(len(x2)))

    cc = np.zeros(nbins)
    _tpc.wtpcc(cc, x1, y1, z1, w1, x2, y2, z2, w2, maxangle)

    if kwargs.get("cumulative", True):
        cc = np.cumsum(cc)
    if kwargs.get("normalized", False):
        cc /= sum(w1) * sum(w2)
    return cc

def tpcc_healpix(m1, m2, maxangle, nbins, **kwargs):
    """
    Calculates the angular two-point correlation between 2 HEALpix maps
    """
    npix = len(m1)
    nside = healpy.npix2nside(npix)
    x, y, z = healpy.pix2vec(nside, range(npix))

    # limit the calculation to non-zero elements
    i1 = (m1 != 0)
    i2 = (m2 != 0)

    # wtpcc doesn't work on int arrays
    w1 = m1[i1].astype(float)
    w2 = m2[i2].astype(float)
    cc = np.zeros(nbins)

    _tpc.wtpcc(cc, x[i1], y[i1], z[i1], w1,
                   x[i2], y[i2], z[i2], w2, maxangle)

    if kwargs.get("cumulative", True):
        cc = np.cumsum(cc)
    if kwargs.get("normalized", False):
        cc /= sum(m1) * sum(m2)
    return cc


def thpac(x, y, z, zetabins=None, gammabins=None, **kwargs):
    """
    Angular three-point auto-correlation with optional weights.

    Parameters
    ----------
    maxangle: float
        maximum angular separation in [rad]
    nbins: int
        number of angular bins
        bin edges are given by linspace(0, maxangle, nbins+1)
    weights: array_like, optional
        weight for each event
    cumulative: bool
        cumulative autocorrelation, default: True
    normalized: bool
        normalize S(pi) = 1, default: False

    Returns
    -------
    ac: ndarray
        histogram of the angular distances of pairs of vectors x,y,z

    See Also
    --------
    astrotools.twoPtAuto
    """
    
    gammabins = np.linspace(-5, 5, 51, endpoint=True)
    zetabins = np.linspace(0, 20, 41, endpoint=True)


    # optional weights
    w = kwargs.get('weights', np.ones(len(x)))

    hist = np.zeros((len(zetabins), len(gammabins)))
    _tpc.wthpac(hist, x, y, z, w, zetabins, gammabins)

    if kwargs.get("normalized", False):
        hist /= (sum(w)**2 - sum(w**2)) / 2
    return hist, gammabins, zetabins